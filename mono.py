import sys,random,time
from util import filterForPlayerNumber, WELCOMETEXT, get_random_animal_in_random_color, random_word

TYPING_SPEED = 300 # value during development
def slow_type(t, typing_speed=TYPING_SPEED):
    for l in t:
        sys.stdout.write(l)
        sys.stdout.flush()
        time.sleep(random.random()*10.0/typing_speed)
    print('')

def fast_type(t, speed=500):
    slow_type(t, speed)


runden_dauer = {
    "kurz": 0,
    "mittel": 1,
    "lang": 2,
    "oberlang":3
}

platz = ["m^2", "tisch", "draussen", "drinnen", "gequetscht", "welt"]
kick = ["memory", "creativity", "quatsch", "wissen", "lachen", "erklaeren", "intimacy"]
alter = ["kind", "jugendlicher", "grownup"]
need = ["karten", "papier", "internet", "nix"]
competition =  ["cooperative", "competetitive", "wegalsziel"]
difficulty = ["simple", "complicated", ""]
sex = ["naugthy", "jugendfrei"]
alkohol = ["saufen", "abstinent"]
mindestPersonen = ["2p", "3p", "4p", "5p"]
drogen = ["kiffaffe", "speedaffe", "lsdaffe", "pilzaffe", "keinaffe"]  # TODO these tags have to be sorted into the games

# gameName: list mit antworten von fragen
games = {
    "Contact": [3, "kurz", "quatsch", "kreativ", "keinePlatz", "schreibenKoennen"],
    "1,2,3": [5, "m^2", "draussen", "naugthy", "wegalsziel", "intimacy", "creativity", "grownup", "jugendlicher", "nix", "5p"],
    "TierDuell": [3, "m^2", "lachen", "kind", "nix", "wegalsziel", "jugendfrei", "entscheiden", "3p"],
    "OpenSchnick": [2, "welt", "simple", "creativity", "entscheiden"],
    " Quaters": [5, "mittel", "tisch", "jugendlicher", "saufen", "competetive", "naughty"]
}

# DIESE Fragen koennen gestellt werden.
# frage: liste mit antwortmoeglichkeiten
fragen = {
    "Willst du etwas entscheiden? zB wer den Muell runterbringen soll?": ["entscheiden"],
    "Wieviel Platz habt ihr?": platz,
    "Worauf hast du Lust?": kick,
    "Was habt ihr griffbereit?": need,
    "Wollt ihr zusammen oder gegeneinander spielen?": competition,
    "Kann es auch um Sex gehen?": sex,
    "Wollt ihr saufen?": alkohol,
    "Welche Affen spielen mit?": drogen
    }

# HUGO: Sollen wir einen "Buntebildergenerator" basteln? Ich hab überlegt, ob man "Drogen nehmen" noch irgendwie in die Beschreibung mit einbaut. Und dann könnte man vielleicht einen Screen mit sich wiederholenden bunten Bildern oder so laufen lassen. Dafür die Affenfrage. Hab noch keine guten Antworten. 


def get_game_characteristics(game):
    return games[game]

def get_question_for_game(gameName, questions):
    foundQ = False
    while not foundQ:
        random_property_of_game = random.choice(get_game_characteristics(gameName))
        # print("rand prop of game",  gameName, ": ", random_property_of_game)
        for frage in fragen:
            possible_answers = fragen[frage]
            if random_property_of_game in possible_answers:
                return frage, possible_answers

def game_has_any_property_of_question(gameName, options):
    for p in get_game_characteristics(gameName):
        if p in options:
            return True, p
    return False, None

def get_mono_choice(options):
    a = None
    while a not in options and a != 88:
        try:
            a = int(input())
        except:
            print("Please enter one of the following options (or 88):", options)
    return a


def filter_by_selector(tag, games, verbose=False):
    valid_games = []
    for title in games:
        if tag in get_game_characteristics(title):
            valid_games.append(title)
    if verbose:
        print("pre- and post-selection for %s : %s -> %s" % (tag, len(games), len(valid_games)))
    return valid_games #, len(books), len(valid_books)



# reduce a list of games by asking a question
def filter_with_question(question, options, valid_games):
    slow_type(question)
    possible_answers = {}
    # presenting the answer options as dictacted by the valid games left
    for idx, gameName in enumerate(valid_games):
        canBeFilteredFor, withAnswer = game_has_any_property_of_question(gameName, options)
        if canBeFilteredFor and withAnswer not in possible_answers.values():
            fast_type("(%s) %s" % (idx, withAnswer))
            possible_answers.update({idx: withAnswer})
    fast_type("(88) egal / andere Frage, bitte")
    # print(possible_answers)


    # get answer and filter out games that have it
    choice = get_mono_choice(possible_answers)
    print("mono chose...", possible_answers[choice])
    slow_type(random_word())
    if choice == 88:
        return valid_games
    else:
        return filter_by_selector(possible_answers[choice], valid_games)




slow_type(get_random_animal_in_random_color(), 20000)
slow_type("Hallo Mono...")
time.sleep(0.5)
slow_type(WELCOMETEXT)
slow_type("Wieviele Leute seid ihr?")
a = int(input())
valid_games = filterForPlayerNumber(games, a)

a = True
i = 0
while len(valid_games) > 1 and a:
    # print("iteration", i, " valid_games", valid_games)
    random_game_name = random.choice(valid_games)
    # print('random game name', random_game_name)
    # print ("getting a question for that:")
    (question, options) = get_question_for_game(random_game_name, fragen)
    # print("fitler after that q")
    valid_games = filter_with_question(question, options, valid_games)


slow_type("Supi:")
slow_type("Na dann haben wir folgendes fuer dich...")
slow_type(valid_games[0])

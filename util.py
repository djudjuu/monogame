import random

def filterForPlayerNumber(listOfGames, number):
    valid_games = []
    for gname in listOfGames:
        # print("looking at", games[game])
        # print("looking at2", listOfGames[gname])
        # print("looking at2", listOfGames[gname][0])
        if listOfGames[gname][0] <= number:
            valid_games.append(gname)
    return valid_games

# enclosing a string in those makes terminal output colorful
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

adjectives = ["brave", "iiiinteresting", "aha", "hmmm", "nice", "let's see", "who would have thought that?", "(REALLY?)", "okay", "let's go", "oh yeah", "as you wish", "como quiser"]
def random_word():
    return random.choice(adjectives)



WELCOMETEXT = """Hallo Mono, \n kennst du das Sprichwort 'Wer gibt bekommt nichts zurück?' \n
wir auch nicht. Deshalb konnten wir uns auch nicht daran halten. Da uns
offensichtlich allen diese Programmiererei so einen Spaß bereitet, sahen wir
uns gezwungen dir etwas zurück zu basteln. Du bist herzlich eingeladen selbst
mit dran herumzutüfteln und nach Belieben zu ergänzen. Jetzt fragst du dich
sicherlich noch, was dieses Ding denn kann. Natürlich sollst du eine Antwort
bekommen! Du hälst, gewissermaßen 'Spielzeit' in deinen Händen: Falls du einmal
nicht genau weißt, wie du dir die Zeit vertreiben sollst, kannst du einfach
diesen Spielegenerator befragen und er wird dir schon etwas passendes
ausspucken. Egal ob allein oder mit vielen Menschen, er ist multifunktional. \n
\n In diesem Sinne...Gutes Spiel!"""

SNAKE = """

                                     ..:::::;'|
             __   ___             / \:::::;'  ;
           ,::::`'::,`.          :   ___     /
          :_ `,`.::::)|          | ,'SSt`.  /
          |(` :\)):::`;          |:::::::| :
          : \ ,`'`:::::`.        |:::::::| |
           \ \  ,' `:::::`.      :\::::::; |
            \ `.  ,' ` ,--.)     : `----'  |
             :  `-.._,'__.'      :   ____  |
             |     |              :,'::::\ |
             :  _  |              :::::::::|
             ;     :              |:::::::||
            :      |              |:::::::;|
            :  __  :              |\:::::; |
            |      |              | _____  |
            |      :              |':::::\ |
            : .--  |\             |::::::::|
            :      :.\            |:::::::;|
             :      \:\           |::::::/ |
              \ .-'  \'`.         ;`----' /;
               \      \|::-...__,'._     //
                `.  ,' `:::/ |::::::`. ,'/
                  `.     `-._;:::::::.','
                    `-..__,   ``--'' ,'
                          ``---....-'
"""


TIGER1 = """
                                       _
                                     ,',\ 
                                    ( / \\
                                   __)\);.)
                              _.-''  ,;',:.`-.
                            ,'       ::(o))`. `.
                           /          \`,',',\ `.
                          /  ,;''._ /.--.._' /,'`.
                       __/   /,'o\ \'.   `.`,'  `.
                    ,';.,  `.`..--'/.`     `..:`'.
                   `. `: \ `.`-   : |       |:'\-.`
                 _.-;`-|  `. `-.  |.:      ,'|  \ `
              ,-'; /,' ;|   `.__.;', `.,--'\(;   `._
            ,'; : /:  :`'.  _.-' .'    `-.:.: __    `.
           / ; : : :  :  `/.   ,'-,'        ,` _`-._  \ 
          / ; :  | :   . ,`'|,/','| \      /_,'_|>'   /
         ; :  :  |  .  ,'      /  ; |\    ;_.-'     .'
        :  :  :  :   ./       /  /  ; \         _.-'
        :  :  :   `  /          /  :   `\,.__.-'
        |  :  :     :         ,'   |
        |  :  :     |        ;,.   ;
        :  :   . :\ |        `  : :
        ;  :   : | `'           | |
      ,'|   .   .|              ; ;
    ,'; :   :    |             : :
  ,; ; ';  . .SSt;/            ; |
"""

TIGER2 = """
                        _____
            __       .-'`     `'-.       __
           / _'-._.-'--,       ,--'-._.-'_ \ 
          | / ' .'  .--.\     /.--.  '. ' \ | 
          ||  .' .:/____\\   //____\:. '.  ||
           \,/.   |  \o/ |   | \o/  |   .\,/
         ,-`/ :.   '.___.|   |.___.'   .: \`-,
       ,--`/.  '::'    / /   \ \    '::'  .\`--,
     ,'--'| :.       /` /     \ `\       .: |'--',
     ,'`.  \ ':.:' /`  (`'---'`)  `\ ':.:' /  `',
     ,'  _  '-..--'|  . '.   .' .  |'--..-'   ',
      `.-`         \  .'  `:`  '.  /         `-.`
      `.-'_  .  _   \,__.--'--.__,/  __ . _'-.`
         ' `/.-' ''-)            (-''  '-.\`
                    |            |
                    |            |
                    \            /
                     '.________.'

"""

BAGIRA = """
                ,--"'"--,
               .'_       _'.
        .-"-._/ / \     / \ \_.-"-.
       /.-,  /  \(o_/ \_o)/  \  ,-.\ 
       |   >/     _/   \_     \<   |
       \  ` |  .`( '-.-' )'.  | `  /
        '--'|  |  '-. .-'  |  |'--'
            |  | .'. | .'. |  |
            \   \___.'.___/   /
             '.  )\/-.-\/(  .'
               \/'./\_/\.'\/
                |  `"`"`  |
                |         |
                \         /
                 '._____.'

"""

jungleAnimals = [SNAKE, BAGIRA, TIGER2, TIGER1]
def get_random_animal_in_random_color():
    animal_colors = [bcolors.OKBLUE, bcolors.OKGREEN, bcolors.WARNING, bcolors.FAIL]
    colored_animal = random.choice(animal_colors) + random.choice(jungleAnimals) + bcolors.ENDC
    return colored_animal

